package com.codemotions.keycloak.providers;

import org.keycloak.hash.PasswordHashProvider;
import org.keycloak.models.UserCredentialModel;
import org.keycloak.models.UserCredentialValueModel;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Sha256Base64PasswordHashProvider implements PasswordHashProvider {

    private static final String DIGEST_ALGORITHM = "SHA-256";
    private static final String PASSWORD_CHARSET_NAME = "UTF-8";

    private final String providerId;

    public Sha256Base64PasswordHashProvider(final String providerId) {
        this.providerId = providerId;
    }

    @Override
    public UserCredentialValueModel encode(final String rawPassword, final int iterations) {
        final String encodedPassword = encode(rawPassword);
        final UserCredentialValueModel credentials = new UserCredentialValueModel();
        credentials.setAlgorithm(providerId);
        credentials.setType(UserCredentialModel.PASSWORD);
        credentials.setHashIterations(iterations);
        credentials.setValue(encodedPassword);
        return credentials;
    }

    @Override
    public boolean verify(final String rawPassword, final UserCredentialValueModel credential) {
        return encode(rawPassword).equals(credential.getValue());
    }

    @Override
    public void close() {

    }

    private String encode(final String rawPassword) {
        try {
            final MessageDigest md = MessageDigest.getInstance(DIGEST_ALGORITHM);
            final Base64.Encoder encoder = Base64.getEncoder();
            return encoder.encodeToString(md.digest(rawPassword.getBytes(PASSWORD_CHARSET_NAME)));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new RuntimeException("Credential could not be encoded");
        }
    }
}
