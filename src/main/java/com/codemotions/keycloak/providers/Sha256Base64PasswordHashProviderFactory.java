package com.codemotions.keycloak.providers;

import org.keycloak.Config;
import org.keycloak.hash.PasswordHashProvider;
import org.keycloak.hash.PasswordHashProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class Sha256Base64PasswordHashProviderFactory implements PasswordHashProviderFactory {

    private static final String ID = "md5base64";

    @Override
    public PasswordHashProvider create(final KeycloakSession session) {
        return new Sha256Base64PasswordHashProvider(ID);
    }

    @Override
    public void init(final Config.Scope scope) {

    }

    @Override
    public void postInit(final KeycloakSessionFactory keycloakSessionFactory) {

    }

    @Override
    public void close() {

    }

    @Override
    public String getId() {
        return ID;
    }
}
